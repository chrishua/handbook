---
title: "Architecture Design Documents"
no_list: true
---

Design documents are the primary artifact that the [architecture design workflow](../workflow/)
revolves around. A design document describes a technical vision and a set of principles
that will guide feature implementation, as we move forward. It acts as guardrails
to keep team aligned.

They are version controlled documents that are constantly updated with new insights and knowledge,
after every iteration, to become even more useful with time.

## Contributing

At GitLab, everyone can contribute, including to our design documents. If you would like to contribute to any of these documents, feel free to:

<!-- TODO: Add link to the repository directory once the first design document has been merged. -->
1. Go to the source files in the repository and select the design document you wish to contribute to.
1. [Create a merge request](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).
1. `@` message both an author and a coach assigned to the design document, as listed below.

{{% alert color="primary" %}}
We plan to move the architecture and design documents section [from the docs page](https://docs.gitlab.com/ee/architecture/) to this place.
During the migration this list of design documents will be incomplete.
Follow [the migration issue](https://gitlab.com/gitlab-com/content-sites/handbook/-/issues/279) for updates.
{{% /alert %}}

{{< design-documents-list folder="handbook/engineering/architecture/design-documents" >}}
