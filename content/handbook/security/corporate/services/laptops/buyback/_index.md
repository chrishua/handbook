---
title: Laptop Buyback
---

## Overview

If the team member has not completed 1 calendar year at the time of offboarding or has received a laptop refresh within the past year, they have the option to purchase their laptop for current market value from GitLab. If the laptop has been used by the team member for more than one year, they can opt to keep their laptop at no cost, subject to promptly working with the IT Analyst team during the offboarding process to wipe the laptop and removed from management system, when required by applicable legal obligations, imaged for preservation purposes.

If the team member would like to purchase the laptop at the current market value, they will need to send an email to laptops@gitlab.com to start the process. If purchasing, our Manager of IT, or Lead IT Analyst will approve, and we will send the employee an email with the determined value. If the employee decides to move forward with purchasing, our accounting department will reach out with payment information.

Retained laptops must follow the [wipe process](/handbook/security/corporate/services/laptops/wipe).

If the departing team member opts not to keep or [donate](/handbook/security/corporate/services/laptops/donation) their laptop, they can [return](/handbook/security/corporate/services/laptops/recycle) it to GitLab.
